package SLERP;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import java.awt.GridLayout;
import javax.sql.RowSet;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class QuaternionFrame extends JFrame {

    private JTextField x1, x2, y1, y2, z1, z2, w1, w2;

    public QuaternionFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 150);
        int rows = 4, cols = 4;
        setLayout(new GridLayout(rows, cols));
        //row 1
        add(new JLabel("X1"));
        add(new JLabel("Y1"));
        add(new JLabel("Z1"));
        add(new JLabel("W1"));
        //row 2
        add(x1 = new JTextField("0"));
        add(y1 = new JTextField("0"));
        add(z1 = new JTextField("1"));
        add(w1 = new JTextField("3.14"));
        //row 3
        add(new JLabel("X2"));
        add(new JLabel("Y2"));
        add(new JLabel("Z2"));
        add(new JLabel("W2"));
        //row 4
        add(x2 = new JTextField("0"));
        add(y2 = new JTextField("1"));
        add(z2 = new JTextField("0"));
        add(w2 = new JTextField("3.14"));
        setVisible(true);
    }

    public void set(Vector3f origin, Quaternion q1, Quaternion q2) {
        try {
            q1.fromAngleAxis(Float.parseFloat(w1.getText()), new Vector3f(Float.parseFloat(x1.getText()), Float.parseFloat(y1.getText()), Float.parseFloat(z1.getText())));
            q2.fromAngleAxis(Float.parseFloat(w2.getText()), new Vector3f(Float.parseFloat(x2.getText()), Float.parseFloat(y2.getText()), Float.parseFloat(z2.getText())));

//            System.out.println(q1);
//            System.out.println(q2);
//            System.out.println(origin);
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }
}
