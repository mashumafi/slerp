package SLERP;

import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.debug.Arrow;
import com.jme3.scene.shape.Line;
import com.jme3.system.AppSettings;
import java.awt.event.WindowEvent;

public class Main extends SimpleApplication {

    public static final Vector3f TRANS_VECTOR = new Vector3f(0f, 5f, 0f);
    private Spatial oto;
    private boolean toStart;
    private Spatial start, end;
    private Quaternion sq, eq, iq;
    private Vector3f origin;
    private ControlJFrame controlFrame;
    private float rotateProgress;
    private boolean initialized = false;
    private Material matRed;
    private Material matGreen;
    private Material matBlue;
    private Material startMat;
    private Material endMat;
    private Material transitionMat;

    public static void main(String[] args) {
        Main app = new Main();
        app.setShowSettings(false);
        app.setDisplayFps(false);
        app.setDisplayStatView(false);

        AppSettings settings = new AppSettings(true);
        settings.setResolution(640, 480);
        settings.setFrameRate(60);
        settings.setVSync(true);
        app.setSettings(settings);
        app.setPauseOnLostFocus(false);

        app.start();
    }

    @Override
    public void simpleInitApp() {
        controlFrame = new ControlJFrame();
        controlFrame.setVisible(true);
        initMaterials();
        initStartStop();
        initTransitional();
        initLights();
        initFlyCam();
        initCoordCross();
        rotateProgress = 0f;
    }

    public void initFlyCam() {
        flyCam.setMoveSpeed(10);
        flyCam.setRotationSpeed(5);
        flyCam.setEnabled(true);
        flyCam.setDragToRotate(true);
        cam.setLocation(new Vector3f(-5f, 1.5f, 25f));
        cam.lookAt(oto.getWorldTranslation(), Vector3f.UNIT_Y);
    }

    public void initMaterials() {
        matRed = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        matRed.setColor("Color", ColorRGBA.Red);
        matGreen = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        matGreen.setColor("Color", ColorRGBA.Green);
        matBlue = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        matBlue.setColor("Color", ColorRGBA.Blue);

        startMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        startMat.setBoolean("UseMaterialColors", true);
        startMat.setColor("Specular", ColorRGBA.White);
        startMat.setColor("Diffuse", ColorRGBA.Blue);
        //mat1.setColor("NormalMap", ColorRGBA.White);
        startMat.setFloat("Shininess", 5f);

        endMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        endMat.setBoolean("UseMaterialColors", true);
        endMat.setColor("Specular", ColorRGBA.White);
        endMat.setColor("Diffuse", ColorRGBA.Red);
        //mat2.setColor("NormalMap", ColorRGBA.White);
        endMat.setFloat("Shininess", 5f);

        transitionMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        transitionMat.setBoolean("UseMaterialColors", true);
        transitionMat.setColor("Specular", ColorRGBA.White);
        transitionMat.setColor("Diffuse", ColorRGBA.Green);
        //mat3.setColor("NormalMap", ColorRGBA.White);
        transitionMat.setFloat("Shininess", 5f);
    }

    public void initStartStop() {
        //Geometry g = new Geometry("start shape", new Cylinder(18, 18, 0.25f, 3, true));
        Spatial g = assetManager.loadModel("Models/Oto/Oto.mesh.xml");
        g.setMaterial(startMat);
        g.setLocalTranslation(TRANS_VECTOR);
        Node n = new Node("start node");
        n.attachChild(g);
        start = n;
        rootNode.attachChild(start);

        //g = new Geometry("end shape", new Cylinder(18, 18, 0.25f, 3, true));
        g = assetManager.loadModel("Models/Oto/Oto.mesh.xml");
        g.setMaterial(endMat);
        g.setLocalTranslation(TRANS_VECTOR);
        n = new Node("end node");
        n.attachChild(g);
        end = n;
        rootNode.attachChild(end);

        sq = new Quaternion();
        iq = new Quaternion();
        eq = new Quaternion();

        origin = Vector3f.ZERO;
    }

    public void initTransitional() {
        //rootNode.attachChild(oto = assetManager.loadModel("Models/Oto/Oto.mesh.xml"));

        //Geometry g = new Geometry("box", new Cylinder(18, 18, 0.25f, 3, true));
        Spatial g = assetManager.loadModel("Models/Oto/Oto.mesh.xml");
        g.setMaterial(transitionMat);
        g.setLocalTranslation(TRANS_VECTOR);
        Node n = new Node("trans node");
        n.attachChild(g);
        oto = n;
        rootNode.attachChild(oto);
    }

    public void initLights() {
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.Gray.mult(5f));
        rootNode.addLight(ambient);

        /**
         * A white, directional light source
         */
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection((new Vector3f(-0.5f, -0.5f, -0.5f)).normalizeLocal());
        sun.setColor(ColorRGBA.White.mult(1.5f));
        rootNode.addLight(sun);

        DirectionalLight sun2 = new DirectionalLight();
        sun2.setDirection((new Vector3f(0.5f, 0.5f, 0.5f)).normalizeLocal());
        sun2.setColor(ColorRGBA.Yellow);
        rootNode.addLight(sun2);
    }

    private void initCoordCross() {
        Line xAxis = new Line(new Vector3f(-3, 0, 0), Vector3f.ZERO);
        Line yAxis = new Line(new Vector3f(0, -3, 0), Vector3f.ZERO);
        Line zAxis = new Line(new Vector3f(0, 0, -3), Vector3f.ZERO);
        Arrow ax = new Arrow(new Vector3f(3, 0, 0));
        Arrow ay = new Arrow(new Vector3f(0, 3, 0));
        Arrow az = new Arrow(new Vector3f(0, 0, 3));
        Geometry geomX = new Geometry("xAxis", xAxis);
        Geometry geomY = new Geometry("yAxis", yAxis);
        Geometry geomZ = new Geometry("zAxis", zAxis);
        geomX.setMaterial(matRed);
        geomY.setMaterial(matGreen);
        geomZ.setMaterial(matBlue);
        Geometry geomXA = new Geometry("xAxis", ax);
        Geometry geomYA = new Geometry("yAxis", ay);
        Geometry geomZA = new Geometry("zAxis", az);
        geomXA.setMaterial(matRed);
        geomYA.setMaterial(matGreen);
        geomZA.setMaterial(matBlue);
        rootNode.attachChild(geomX);
        rootNode.attachChild(geomY);
        rootNode.attachChild(geomZ);
        rootNode.attachChild(geomXA);
        rootNode.attachChild(geomYA);
        rootNode.attachChild(geomZA);
    }

    @Override
    public void simpleUpdate(float tpf) {
        if (!initialized) {
            inputManager.deleteMapping("FLYCAM_Forward");
            inputManager.deleteMapping("FLYCAM_Backward");
            inputManager.addMapping("FLYCAM_Rise", new KeyTrigger(KeyInput.KEY_W));
            inputManager.addMapping("FLYCAM_Lower", new KeyTrigger(KeyInput.KEY_S));
            initialized = true;
        }
        sq = controlFrame.getStartQuaternion();
        eq = controlFrame.getEndQuaternion();

        start.setLocalTranslation(origin);
        start.setLocalRotation(sq);
        end.setLocalTranslation(origin);
        end.setLocalRotation(eq);

        Quaternion to;
        rotateProgress += tpf * 1 / 4;
        if (toStart) {
            iq.slerp(eq, sq, rotateProgress);
            to = sq;
        } else {
            iq.slerp(sq, eq, rotateProgress);
            to = eq;
        }
        if (rotateProgress >= 1.0f) {
            toStart = !toStart;
            rotateProgress = 0;
        }
        oto.setLocalRotation(iq);
        cam.lookAt(oto.getWorldTranslation(), Vector3f.UNIT_Y);
    }

    @Override
    public void destroy() {
        super.destroy();
        //System.out.println("shutting down");
        controlFrame.dispatchEvent(new WindowEvent(controlFrame, WindowEvent.WINDOW_CLOSING));
    }
}
